import zipfile
import json
import os
import shutil
import pandas
import sqlalchemy as db
from sqlalchemy.sql import table, column, select, update, insert, text
engine = db.create_engine('mysql+pymysql://root:JYF958zef++.@localhost:3306/flowlity')

with open('config.json') as json_file:
    config = json.load(json_file)

path = config['path_data']
zip_to_move = []
dir_to_delete = []

def insert_data(day):
    products = []
    print(day)
    # On met a jour le referentiel produit
    for file in os.listdir(path + "\\" + day):
        df = pandas.read_csv(path + "\\" + day+ "\\" + file, delimiter=";")
        for p in df['ProductId']:
            products.append(p)
        
    # dedoublonnage
    products = list(dict.fromkeys(products))
    
    # recuperation du referentiel et insertion en base du reste
    conn = engine.connect()
    query = conn.execute("select * from product;")
    productRef = []
    existingProducts = []
    for i in query.cursor.fetchall():
        productRef.append(i)
        existingProducts.append(i[1])

    for p in products:
        if p not in existingProducts:
            insstmt = text('''insert into product (name) values (:pro)''')
            txn = conn.begin()
            result = conn.execute(insstmt, pro=p)
            recid = conn.execute(text('SELECT LAST_INSERT_ID()')).fetchone()[0]
            productRef.append({recid,p})
            txn.commit()

    # ref sous forme de dataframe
    pr = pandas.DataFrame(productRef, columns=['productSid', 'pName'])

     # On insere les donnees de chaque entite avec id technique produit
    for file in os.listdir(path + "\\" + day):
        df = pandas.read_csv(path + "\\" + day+ "\\" + file, delimiter=";")
        # Jointure avec le ref produit
        dfJoin = pandas.merge(df,pr, left_on='ProductId', right_on='pName').fillna(value=0)
        if file == 'Demand.csv':
            df_dmd = dfJoin.drop(columns=['ProductId'])
            df_dmd.to_sql(con=engine, name='demand', if_exists='replace')

        if file == 'DemandForecast.csv':
            df_dmdfor = dfJoin.drop(columns=['ProductId'])
            df_dmdfor['month'] = pandas.DatetimeIndex(df_dmdfor['Date']).month
            df_dmdfor['Year'] = pandas.DatetimeIndex(df_dmdfor['Date']).year
            #df_dmdfor['week'] = df_dmdfor['Date'].week
            df_dmdfor.to_sql(con=engine, name='demandforecast', if_exists='replace')

        if file == 'InventoryLvl.csv':
            df_inv = dfJoin.drop(columns=['ProductId'])
            df_inv.to_sql(con=engine, name='inventorylvl', if_exists='replace')

        if file == 'Supply.csv':
            df_inv = dfJoin.drop(columns=['ProductId'])
            df_inv.rename(columns={'year':'Year'}, inplace=True)
            df_inv.to_sql(con=engine, name='supply', if_exists='replace')

for file in os.listdir(path):
    if file.endswith(".zip"):
        with zipfile.ZipFile(os.path.join(path, file), 'r') as zip_ref:
            day = file.split('_')[0]
            os.mkdir(path + "\\" + day )
            zip_ref.extractall(path + "\\" + day )
            insert_data(day)
            zip_to_move.append(file)
            dir_to_delete.append(day)


# On deplace le zip dans inserted
for file in zip_to_move:
    os.rename(path + "\\" + file, path + "\\inserted\\" + file)

# On supprime le rep unzippe temporaire
for dir in dir_to_delete:
    shutil.rmtree(path + "\\" +day)





