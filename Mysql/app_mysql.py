from flask import Flask, request
from flask_restful import Resource, Api
import sqlalchemy as db
from sqlalchemy.sql import table, column, select, update, insert, text
from json import dumps
from flask_jsonpify import jsonify
import pandas

engine = db.create_engine('mysql+pymysql://root:JYF958zef++.@localhost:3306/flowlity')
app = Flask(__name__)
api = Api(app)

class get_data(Resource):
    def get(self, productid):
        
        sources = [ "demand","demandForecast","supply","inventoryLvl"]
        periodType = "day"
        periods =  ["04/06/2020","05/06/2020","06/06/2020"]

        result = {
                    'sources' : sources, 
                    'productid' : productid,
                    'periodType' : periodType,
                    'periods' : periods,
                    'data' : {}
                }

        if 'demand' in sources:
            queryString = '''SELECT * FROM demand where productSid = %s'''
            queryParams = {productid}
            demandDf = pandas.read_sql(sql = queryString, con=engine,parse_dates=['Date'], params = queryParams)
            demandRes = []
            if periodType == "day":
                demandDf = demandDf[demandDf.Date.isin(periods)]
                for index, row in demandDf.iterrows():
                    demandRes.append(row['Quantity'])
            if periodType == "year":
                demandDf = demandDf[demandDf.Year.isin(periods)]
                qty = demandDf.groupby(['Year'])['Quantity'].sum().reset_index()
                demandRes.append(qty['Quantity'][0])
            if periodType == "month":
                demandDf = demandDf[demandDf.month.isin(periods)]
                demandDf = demandDf.groupby(['month'])['Quantity'].sum().reset_index()
                demandRes.append(qty['Quantity'][0])
            if periodType == "week":
                demandDf = demandDf[demandDf.week.isin(periods)]
                demandDf = demandDf.groupby(['week'])['Quantity'].sum().reset_index()
                demandRes.append(qty['Quantity'][0])
            result['data']['demand'] = {}
            result['data']['demand']["values"] = demandRes

        if 'demandForecast' in sources:
            demFctRes = []
            queryString = '''SELECT * FROM demandForecast where productSid = %s'''
            queryParams = {productid}
            demFctDf = pandas.read_sql(sql = queryString, con=engine,parse_dates=['Date'], params = queryParams)
            if periodType == "day":
                demFctDf = demFctDf[demFctDf.Date.isin(periods)]
                for index, row in demFctDf.iterrows():
                    demFctRes.append(row['Forecast'])
            if periodType == "year":
                demFctDf = demFctDf[demFctDf.Year.isin(periods)]
                qty = demFctDf.groupby(['Year'])['Forecast'].sum().reset_index()
                demFctRes.append(qty['Forecast'][0])
            if periodType == "month":
                demFctDf = demFctDf[demFctDf.month.isin(periods)]
                qty = demFctDf.groupby(['month'])['Forecast'].sum().reset_index()
                demFctRes.append(qty['Forecast'][0])
            if periodType == "week":
                demFctDf = demFctDf[demFctDf.week.isin(periods)]
                qty = demFctDf.groupby(['week'])['Forecast'].sum().reset_index()
                demFctRes.append(qty['Forecast'][0])
            
            result['data']['demandForecast'] = {}
            result['data']['demandForecast']["values"] = demFctRes

        if 'inventoryLvl' in sources:
            invRes = []
            queryString = '''SELECT * FROM inventorylvl where productSid = %s'''
            queryParams = {productid}
            invDf = pandas.read_sql(sql = queryString, con=engine,parse_dates=['Date'], params = queryParams)
            if periodType == "day":
                invDf = invDf[invDf.Date.isin(periods)]
                for index, row in invDf.iterrows():
                    invRes.append(row['Quantity'])
            if periodType == "year":
                invDf = invDf[invDf.Year.isin(periods)]
                qty = invDf.groupby(['Year'])['Quantity'].sum().reset_index()
                invRes.append(qty['Quantity'][0])
            if periodType == "month":
                invDf = invDf[invDf.month.isin(periods)]
                qty = invDf.groupby(['month'])['Quantity'].sum().reset_index()
                invRes.append(qty['Quantity'][0])
            if periodType == "week":
                invDf = invDf[invDf.week.isin(periods)]
                qty = invDf.groupby(['week'])['Quantity'].sum().reset_index()
                invRes.append(qty['Quantity'][0])
            
            result['data']['inventoryLvl'] = {}
            result['data']['inventoryLvl']["values"] = invRes

        if 'supply' in sources:
            supplyRes = []
            queryString = '''SELECT * FROM supply where productSid = %s'''
            queryParams = {productid}
            supplyDf = pandas.read_sql(sql = queryString, con=engine,parse_dates=['Date'], params = queryParams)
            if periodType == "day":
                supplyDf = supplyDf[supplyDf.Date.isin(periods)]
                for index, row in supplyDf.iterrows():
                    supplyRes.append(row['Quantity'])
            if periodType == "year":
                supplyDf = supplyDf[supplyDf.Year.isin(periods)]
                qty = supplyDf.groupby(['Year'])['Quantity'].sum().reset_index()
                supplyRes.append(qty['Quantity'][0])
            if periodType == "month":
                supplyDf = supplyDf[supplyDf.month.isin(periods)]
                qty = supplyDf.groupby(['month'])['Quantity'].sum().reset_index()
                supplyRes.append(qty['Quantity'][0])
            if periodType == "week":
                supplyDf = supplyDf[supplyDf.week.isin(periods)]
                qty = supplyDf.groupby(['week'])['Quantity'].sum().reset_index()
                supplyRes.append(qty['Quantity'][0])
            result['data']['supply'] = {}
            result['data']['supply']["values"] = supplyRes

        return result

api.add_resource(get_data, '/product/<productid>') 

if __name__ == '__main__':
     app.run(port='5002')