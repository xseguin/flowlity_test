Je pars du principe qu'on travaille sur Azure.

### Can you draw the data pipeline ? ###

Voir pipeline.jpg

### What would be a secure way to receive the daily zip files from customers ? ###
### What do you need to provide to the customer for the daily transfer ? ###
### What do we need to do on our side ? ###

Je propose au client de fournir ces fichiers quotidiens sur un azure blob storage. On peut exiger une connexion sécurisée pour tous les transferts de fichiers sur Blob storage, le client peut se connecter via un Shared access signature token (SAS token).
* De notre côté, il faut créer un container et un token pour le client sur le Storage.
* Il faut fournir au client l’accès au Blob Storage (Connection String + Token).

### How can we process the daily as soon as it arrives ? ###
### How do you test ? ###
### Is there multiple environments (test, dev, prod) ? ###

Le blob storage permet de déclencher un événèment lors de l'arrivée du fichier dans le container. 
Il faut ensuite abonner une azure function qui va intégrer le fichier en base.
On peut aussi éventuellement déclencher un flux Logic apps pour remplacer l'azure function.
Enfin on peut aussi utiliser Azure Data Factory.

On peut utiliser App Insight et créer une alerte qui se base sur une requête de log. Si aucun log n'est trouvé sur une période précise, une alerte se déclenche.

Sinon plus classique, on peut flagger dans une base la journée intégrée, ensuite on peut scheduler une fonction qui va vérifier à une certaine heure que le flag est bien présent.
Si le flag n'est pas présent, elle ira regarder si les données sont présentes en base, au cas contraire vérifier que le fichier a bien été déposé. 
Envoi d'une alerte mail quelque que soit le cas d'erreur. 
En cas de succès, pas d'alerte mail, il ne faut pas envoyer de mails en cas de situation normale, sinon à terme les alertes ne sont plus lues.

Je préconise de faire une intégration brute du fichier, et de faire les transformations dans une seconde partie du pipeline.
Ainsi jusqu'à l'intégration du fichier je propose de n'avoir qu'un environnement de production, sauf si les fichiers fournis peuvent évoluer dans le temps.
Dans le cas où il peut y avoir une évolution des fichiers, je préconise d'avoir effectivement les environnement dev, test et prod.
Aussi si le client va nous envoyer un fichier pour un environnement de recette en plus d'un environnement prod, on crée aussi un environnement de recette.

### How can you monitor? ###
### What are you going to test ? and how? ###
### What would be the behaviour if we receive non expected data ? ###

Il faut faire du fast fail, afin de ne pas perdre de temps, il faut détecter les anomalies prévues au plus tôt et alerter par mail ou notif slack.

La fonction peut vérifier les éléments suivants :
* Si elle arrive à dézipper le fichier
* Si elle trouve tous les fichiers attendus
* Que chaque fichier n'est pas vide
* Que toutes les colonnes prévues sont bien présentes


### What kind of data structure is more suited to store those data ? ###
### For storage, would you choose SQL or noSQL? what could be the advantages/drawback of each? ###

Au vu des données, je pense qu'on peut utiliser les deux sans problèmes. Il n'y a pas particulièrement de complexité dans le modèle des données, ni une volumétrie très importante. 
Il faut cependant penser aux données avec lesquelles on pourrait essayer de les rapprocher, qui peuvent venir d'autres sources, et qui complexifieraient le modèle.

Il y a particulièrment un interêt à utiliser une base NoSql comme MongoDb par exemple si les données étaient du JSON avec de propriétés complexes, comme des listes d'objets. Dans ce cas stocker en 
Sur du CSV, on sait qu'on va avoir pour chaque colonne une valeur simple, donc pas de difficultés à mettre sur une base relationnelle.
Un autre intérêt du NoSql, c'est que si le fichier s'enrichit et que de nouvelles colonnes apparaîssent, on pourra les intégrer dans la même collection sans affecter le modèle.
Sur une base relationnelle il faudra faire évoluer le modèle.

L'intérêt d'une base SQL est de pouvoir faire des jointures entre les tables, ce qui va :

* faciliter l'utilisation d'un référentiel de données, par exemple pour maintenir les informations des produits sur le long terme, par exemple en cas de changement de nom, ou d'autres caractéristiques
* de s'assurer de l'intégrité des données, ce qui rejoint le premier point
* permettre de faire des tables plus légères, car contenant principalement des id pointant sur des tables de référentiel

Je dirais que ce choix dépend : 

* du besoin d'intégrité des données, selon ce qu'on va faire des données (avons-nous besoin d'être sûr que les données ne vont pas changer dans le temps, ou est-ce que cela n’a pas d’importance)
* de la complexité des informations référentielles et de la normalisation des données sources entre elles
* du besoin de faire des jointures pour requêter les données

Je constate que l'Id produit est un libellé, donc je ne peux pas établir un référentiel qui permettrait une évolution des données de référence, par exemple le nom d'un produit ne peut pas changer dans le temps, car je ne peux le ratacher à un autre produit par le biais d'un identifiant unique (integer, UUID ou autre), donc si un produit change de nom, je considère que c'est un nouveau produit.
D’après l’énoncé, L’API doit prendre l’Id du produit en entrée pour faire un GET, donc je dois maintenir un référentiel produit.

### How can we use cache databases to speed up the queries and improve the overall performance? ###

On peut utiliser Redis sur Azure. Il faudrait choisir une clé, par exemple une clé composite id produit + id client, puis pour chaque clé stocker une valeur qui correpondrait à un JSON contenant les valeurs par jour pour une durée limitée, par exemple 1 an glissant.

### What would be your recommendations/advices to improve this data pipeline? ###

Enrichir les données par des informations contextuelles venant d'autres sources.


### (Bonus) How would you deploy a ML model learning on this data and need to predict them daily ? ###

On peut utiliser l'outil Azure ML.
On commence par faire des expérimentations, jusqu'à trouver un modèle qui fonctionne.
Pour cela on peut commencer par importer les data, ce qui peut se faire via un appel à la base, ou à l'API.
On peut s'appuyer sur des templates existants, il existe des templates par exemple pour des forecasts de commandes Retail.
Une fois le modèle sélectionné on peut le déployer en tant que service Web, et créer une fonction qui va l'appeler en lui poussant les données, cette fonction étant déclenchée par l'intégration des données.




